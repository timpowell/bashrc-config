# Adding git information to your bash prompt

Example:
![](figures/git_prompt_example.png)

Add the following to your .bashrc / .bash_profile to add information on what git branch you are on and if you have a clean working directory (branch colour is green), staged but not committed changes (branch colour is yellow), or un-staged changes (branch colour is red) 

Place your current PS1 in `PS1+=`.

```
# Set colours
red=$(tput setaf 9)
amber=$(tput setaf 11)
green=$(tput setaf 10)
reset=$(tput sgr0)

# Determine git status
function git_colour {
    local git_status="$(git status 2>/dev/null)"

    if [[ $git_status =~ "nothing to commit" ]]; then
        echo -e $green
    elif [[ $git_status =~ "Changes to be committed" ]] ; then
         echo -e $amber
    else
        echo -e $red
    fi
}

# Obtain git branch information
parse_git_branch() {
     git branch 2>/dev/null | grep '^*' | colrm 1 2
}

# Set bash prompt
export PS1='\[$(git_colour)\]\[$(printf "%*s\r%s" $(( COLUMNS-1 )) "$(parse_git_branch)")\]\[$reset\]'
PS1+= *YOUR CURRENT PS1 HERE*
```

This code adds git branch information to the right hand side of your bash prompt. To incorporate it into the left hand side replace
```
export PS1='\[$(git_colour)\]\[$(printf "%*s\r%s" $(( COLUMNS-1 )) "$(parse_git_branch)")\]\[$reset\]'
```
with
```
export PS1='\[$(git_colour)\]\[$(parse_git_branch)\]\[$reset\]'
```