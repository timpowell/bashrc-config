# change prompt
# Set colours
purple=$(tput setaf 5)
yellow=$(tput setaf 3)
red=$(tput setaf 9)
amber=$(tput setaf 11)
green=$(tput setaf 10)
reset=$(tput sgr0)
# Determine git status
function git_colour {
    local git_status="$(git status 2>/dev/null)"

    if [[ $git_status =~ "nothing to commit" ]]; then
        echo -e $green
    elif [[ $git_status =~ "Changes to be committed" ]] ; then
         echo -e $amber
    else
        echo -e $red
    fi
}
# Obtain git branch information
parse_git_branch() {
     git branch 2>/dev/null | grep '^*' | colrm 1 2
}
# Set bash prompt
export PS1='\[$(git_colour)\]\[$(printf "%*s\r%s" $(( COLUMNS-1 )) "$(parse_git_branch)")\]\[$reset\]'
PS1+='\[$yellow\]\u\[$reset\]:\[$purple\]\W\[$reset\] $ '
# Without GIT
#PS1='\[$yellow\]\u\[$reset\]:\[$purple\]\W\[$reset\] $ '

# add colour
export CLICOLOR=1
export LSCOLORS=GxFxCxDxBxegedabagaced

# aliases
source ~/.bash_aliases 


